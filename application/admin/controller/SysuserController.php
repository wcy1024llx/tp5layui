<?php

namespace app\admin\controller;

use think\Db;
use think\facade\View;
use app\admin\model\SysuserModel;
use app\common\controller\AdminBaseController;

class SysuserController extends AdminBaseController
{
    public function index()
    {
        return View::fetch();
    }

    public function view()
    {
        $sysuserModel = new SysuserModel();
        $page = input('param.page');
        $pageSize = input('param.pageSize');
        $sysuserlist = $sysuserModel::with("roles")->order("id", "desc")->paginate($pageSize);
        return json($sysuserlist);
    }

    public function add()
    {
        $isPost = $this->request->isPost();
        if ($isPost) {
            $sysuser = new SysuserModel();
            $params = input('post.');
            $params['password'] = md5(123456);
            $params['bind_account'] = $params['account'];
            $roleids = input('post.roleIds');
            if (empty($roleids)) {
                $this->error("请选择权限！", "", $params);
                exit;
            }
            $res = $sysuser->insert($params);
            $userId = Db::name('sysuser')->getLastInsID();
            Db::name('sysrole_user')->where('user_id', $userId)->delete();
            foreach ($roleids as $roleid) {
                $data[] = [
                    'role_id' => $roleid,
                    'user_id' => $userId,
                ];
            }
            Db::name('sysrole_user')->insertAll($data);
            $this->success("成功！", "", $params);
        } else {
            $rolelist = controller("Widget")->widgetRoleList();
            $this->assign("rolelist", $rolelist);
            return View::fetch();
        }
    }

    public function edit()
    {
        $isPost = $this->request->isPost();
        if ($isPost) {
            $params = input('post.');
            $roleids = input('post.roleIds');
            if (empty($roleids)) {
                $this->error("请选择权限！", "", $params);
                exit;
            }
            Db::name('sysuser')->update($params);
            Db::name('sysrole_user')->where('user_id', $params['id'])->delete();
            foreach ($roleids as $roleid) {
                $data[] = [
                    'role_id' => $roleid,
                    'user_id' => $params['id'],
                ];
            }
            Db::name('sysrole_user')->insertAll($data);
            $this->success("成功！", "", $params);
        } else {
            $userid = $this->request->param("id");
            $sysuserModel = new SysuserModel();
            $sysuser = $sysuserModel->find($userid);
            $this->assign("sysuser", $sysuser);

            $rolelist = controller("Widget")->widgetRoleList();
            $this->assign("rolelist", $rolelist);

            $userRoleList = controller("Widget")->widgetUserRoleList($userid);
            $userRoleList = json_encode($userRoleList, JSON_UNESCAPED_UNICODE);
            $this->assign("userRoleList", $userRoleList);
            return View::fetch();
        }
    }

    public function delete()
    {
        $id = input('param.id');
        $sysmodel = new SysuserModel();
        $res = $sysmodel::destroy($id);
        $this->success("成功！", "", $res);
    }
}